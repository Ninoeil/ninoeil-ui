import { Component, output } from '@angular/core';

@Component({
  selector: 'lib-anchor-button',
  standalone: true,
  imports: [],
  templateUrl: './anchor-button.component.html',
  styleUrl: './anchor-button.component.scss',
})
export class AnchorButtonComponent {
  clicked = output();
}
